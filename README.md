# Compression
The File e1120g-2t.cmp is compressed by some unknown (to you) algorithm. Define and provide a solution to decompress  it to match  e1120g-2t.bmp.

## Deliverables:
* Privately fork this repository and commit your work there
* Provide an explanation of how the compression algorithm works
* Include any notes, pseudo code, diagrams you might have produced
* If possible provide working decompression code in Python or C/C++
